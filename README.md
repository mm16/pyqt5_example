# PyQT5 example
Example application of PyQt5 GUI.

## Install
    python3 -m venv venv
    pip install -r requirements.txt

## Run
    python pyqt5_example/main.py

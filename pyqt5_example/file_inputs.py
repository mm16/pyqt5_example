import logging

from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QWidget


LOGGER = logging.getLogger(__name__)


def groupbox():
    def get_file(lineedit: QLineEdit):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            lineedit.setText(filenames[0])
            f = open(filenames[0], "r")

            with f:
                data = f.read()
                LOGGER.error(f"Found file. Content: {data[:min(25, len(data))]} ...")

    groupbox = QGroupBox("File inputs")
    vbox = QVBoxLayout()
    button_texts = ("Get toml file", "Get other file")
    for k, button_text in enumerate(button_texts):
        horizontal_widget = QWidget()
        hbox_per_file = QHBoxLayout()

        lineedit = QLineEdit()
        bt_get_file = QPushButton(button_text)
        # the lambda args are needed to have correct args passed to get_file (otherwise
        # always the last lineedit is used)
        bt_get_file.pressed.connect(lambda le=lineedit: get_file(le))
        hbox_per_file.addWidget(QLabel(f"File {k+1}/{len(button_texts)}"))
        hbox_per_file.addWidget(bt_get_file)
        hbox_per_file.addWidget(lineedit)

        horizontal_widget.setLayout(hbox_per_file)
        vbox.addWidget(horizontal_widget)
    groupbox.setLayout(vbox)
    return groupbox

from PyQt5.QtWidgets import QApplication, QGroupBox, QMainWindow, QVBoxLayout, QWidget
import sys

import file_inputs
import plots

if __name__ == "__main__":

    class Window(QMainWindow):
        def __init__(self):
            super().__init__()

            self.setGeometry(300, 200, 1000, 800)
            self.setWindowTitle("Vienna VLBI Software - Global Solution")
            central_widget = QWidget()
            central_layout = QVBoxLayout()
            central_layout.addWidget(file_inputs.groupbox())
            central_layout.addWidget(plots.button_random())
            central_widget.setLayout(central_layout)

            self.setCentralWidget(central_widget)

    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())

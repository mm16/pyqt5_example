import logging

import matplotlib.pyplot as plt
import numpy as np
from PyQt5.QtWidgets import QPushButton

LOGGER = logging.getLogger(__name__)


def button_random():
    def plot_random():
        len_nums = 5
        x = range(len_nums)
        y = np.random.rand(len_nums)
        LOGGER.debug(f"Plotting {len_nums} random numbers: x={x}, y={y}")
        plt.plot(x, y)
        plt.show()

    pb = QPushButton("Plot random numbers")
    pb.clicked.connect(plot_random)
    return pb
